import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { AddcustomerComponent } from './pages/addcustomer/addcustomer.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [

  {
    path:'',
   component: HomeComponent
  },
  
  {
    path:'addcustomer',
   component: AddcustomerComponent
  },
  {
    path:'viewcustomer',
   component: HomeComponent
  },
  

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
