import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { MainComponent } from './components/main/main.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FooterComponent } from './components/footer/footer.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { HomeComponent } from './pages/home/home.component';
import { AddcustomerComponent } from './pages/addcustomer/addcustomer.component';
import { BusinessCardComponent } from './commons/business-card/business-card.component';
import { IndividualCardComponent } from './commons/individual-card/individual-card.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [MainComponent, SidebarComponent, FooterComponent, TopbarComponent, HomeComponent, AddcustomerComponent, BusinessCardComponent, IndividualCardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,HttpClientModule
  ]
})
export class DashboardModule { }
