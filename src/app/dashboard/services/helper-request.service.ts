import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HelperRequestService {

  constructor(private http: HttpClient) { 

  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  getInsuranceProduct() {
    return this.http.get('https://localhost:5001/api/Helper/insuranceProduct/getAll').pipe(catchError(this.handleError));;
  }

  getRiskCover(filterVal : any){
    return this.http.get('https://localhost:5001/api/Helper/RiskCover/get/'+filterVal).pipe(catchError(this.handleError));;
  }

  getRiskItem(filterVal : any){
    return this.http.get('https://localhost:5001/api/Helper/RiskItem/get/'+filterVal).pipe(catchError(this.handleError));;
  }
}
