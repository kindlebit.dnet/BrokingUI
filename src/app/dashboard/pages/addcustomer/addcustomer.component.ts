import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HelperRequestService } from '../../services/helper-request.service';

@Component({
  selector: 'app-addcustomer',
  templateUrl: './addcustomer.component.html',
  styleUrls: ['./addcustomer.component.css']
})
export class AddcustomerComponent implements OnInit {
  isShown:boolean
  selected:string
  step:number
  products = [];
  riskCovers = [];
  riskItems = [];
  riskRates=[];
  constructor(private dataService: HelperRequestService) { 
    this.selected="Individual";
    this.step=0
  }

  ngOnInit(): void {
    this.isShown= false;
    this.dataService.getInsuranceProduct().subscribe((data: any[])=>{
      console.log(data);
      this.products = data;
    }) 
  }

  toggleShow(){
   // this.isShown=!this.isShown;
    //this.step=this.step+1;
if(this.step!=0){
  this.isShown=true;
}else{
  this.isShown=false;
}
    if(this.step<5){
      this.step=this.step+1
      this.isShown=true;
    }else{
      this.step=5
    }
    console.log(" Value is : ", this.step );
  }

  toggleShowTwo(){
   // this.isShown=!this.isShown;
    if(this.step>0){
      this.isShown=false;
      this.step=this.step-1
      
    }else{
     
      this.step=0
    }
     if(this.step!=0){
      this.isShown=true;
    }else{
      this.isShown=false;
    } 
    console.log(" Value is : ", this.step );
     
   }
  onSubmit(form: NgForm): void {
    return;
  }

  onClick(form: NgForm): void {
    const json = JSON.stringify(form.value);
    console.log(json);

  }

  onItemChange(value){
    this.selected=value;
    console.log(" Value is : ", value );
 }

 filterProducts(filterVal : any){
   
    this.dataService.getRiskCover(filterVal).subscribe((data: any[])=>{
      console.log(data);
      this.riskCovers = data;
    }) 
   
 }
 filterRiskCovers(filterVal : any){
   
  this.dataService.getRiskItem(filterVal).subscribe((data: any[])=>{
    console.log(data);
    this.riskItems = data;
  }) 
 
}

filterRiskItems(filterVal : any){
   
  this.dataService.getRiskItem(filterVal).subscribe((data: any[])=>{
    console.log(data);
    this.riskRates = data;
  }) 
 
}

}
